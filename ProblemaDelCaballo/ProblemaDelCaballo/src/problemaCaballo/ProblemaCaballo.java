package problemaCaballo;

import Pieza.Pieza;
import Tablero.Tablero;
import java.util.Map;
import java.util.LinkedHashMap;

public class ProblemaCaballo {
    private Tablero tablero;
    private Pieza caballo;
    private Map<Integer,Map <String,Integer>> historial;
    
    public ProblemaCaballo(Tablero tablero,Pieza caballo){
        historial =  new LinkedHashMap();
        this.tablero=tablero;
        this.caballo=caballo;
        pintar(caballo.getFila(),caballo.getColumna());
    }
         
    private void pintar(int fila,int columna){
        if(fila<=tablero.getDimension()||columna<=tablero.getDimension()){
            caballo.setFila(fila);caballo.setColumna(columna);
            tablero.marcarCasilla(fila, columna);
            Map<String,Integer> posicion = new LinkedHashMap();
            posicion.put("fila",fila);
            posicion.put("columna",columna);
            historial.put(historial.size()+1, posicion);
            //tablero.recuperarHistorial(problema);
        }
    }
    
    public void moverCaballo (int fila, int columna){
        if(caballo.movimientoPosible(fila, columna))
            pintar(fila,columna);
    }
    
    public Map<Integer,Map <String,Integer>> getHistorial(){
        return historial;
    }
}
