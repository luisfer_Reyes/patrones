/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tablero;

import java.util.LinkedHashMap;
import java.util.Map;
import problemaCaballo.ProblemaCaballo;

/**
 *
 * @author Administrador
 */
public class Tablero {
    private int dimension;
    private Map<String,Boolean> tablero;
    
    public Tablero(int dimension){
        if(dimension < 8)
            this.dimension=8;
        else
            this.dimension=dimension;
        this.tablero=llenarMapa();
    }
    
   
    public int getDimension(){
        return dimension;
    }
    
    
    private void setDimension(int dimension){
        this.dimension=dimension;
    }
    
    public void marcarCasilla(int fila, int columna){
            tablero.put(""+fila+"-"+columna, true);
    }
    
    private Map<String,Boolean> llenarMapa(){
        Map<String,Boolean> tablero=new LinkedHashMap();
        for(int filas=1;filas<=this.dimension;filas++){
            for(int columnas=1;columnas<=this.dimension;columnas++){
                tablero.put(filas+"-"+columnas,false);
            }
        }
        return tablero;
    }
    
    public void recuperarHistorial(ProblemaCaballo problema){
        for(Integer indice = 1; indice <= problema.getHistorial().size(); indice ++){
            String posicion = ""+problema.getHistorial().get(indice).get("fila")+"-"+problema.getHistorial().get(indice).get("columna");
            tablero.put(posicion,true);
        }
        
    }
    
    public void aumentar(Integer cantidadColumnas){
        tablero.clear();
        setDimension(getDimension()+cantidadColumnas);
        for(int filas=1;filas<=this.dimension;filas++){
            for(int columnas=1;columnas<=this.dimension;columnas++){
                tablero.put(filas+"-"+columnas,false);
            }
        }
    }
    
    public void disminuir(Integer cantidadColumnas){
        tablero.clear();
        if (getDimension()-cantidadColumnas <8)
            setDimension(8);
        else
            setDimension(getDimension()-cantidadColumnas);
        
        for(int filas=1;filas<=this.dimension;filas++){
            for(int columnas=1;columnas<=this.dimension;columnas++){
                tablero.put(filas+"-"+columnas,false);
            }
        }
    }
    
    public Map<String,Boolean> getTablero(){
        return tablero;
    }
    
    public void imprimirTablero(){
        System.out.println();
        for(int fila = 1; fila <= dimension;fila++){
            for (int columna = 1; columna <= dimension; columna++){
                String posicion = ""+fila+"-"+columna;
                System.out.print(tablero.get(posicion)+" ");
            }
            System.out.println();
            
        }
    }
}
