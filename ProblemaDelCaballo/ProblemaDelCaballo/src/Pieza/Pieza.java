/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pieza;

import Tablero.Tablero;

/**
 *
 * @author Administrador
 */
public interface Pieza {
    public int getFila();
    public int getColumna();
    public void setFila(int fila);
    public void setColumna(int columna);
    public Boolean movimientoPosible(int fila,int columna);
}
