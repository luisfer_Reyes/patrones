package pieza;

import historial.Casilla;
import tablero.Tablero;
import historial.Historial;


public class Caballo{
    private Casilla casilla; 
    private Tablero tablero;
    protected Historial historial;
    
    public Caballo(Casilla casilla, Tablero tablero, Historial historial){
        if(tablero == null || historial == null || casilla == null)
            throw new IllegalArgumentException();
        this.tablero = tablero;
        if(casilla.getColumna() > tablero.getDimension() || casilla.getFila() > tablero.getDimension()){
            this.casilla = new Casilla(1,1);
        }
        else{
            this.casilla = casilla;
        }
        this.historial = historial;
        historial.agregarAlHistorial(casilla);
    }
    
    public boolean mover(Casilla casilla){
        if(!historial.buscarEnHistorial(casilla)){
            if(!historial.posicionEnHistorial(this.casilla).equals(historial.tamanioHistorial())){
            historial.borrarEnHistorial(historial.posicionEnHistorial(this.casilla)+1);
            }
        
            if(casilla.estaEnRango(tablero) && !historial.buscarEnHistorial(casilla)){
                if(movimientoPosible(casilla)){
                    historial.agregarAlHistorial(casilla);
                    this.casilla=casilla;
                    return true;
                }
            }
        }
        
        return false;
    }
    
    public void regresar(){
        Integer posicion = historial.posicionEnHistorial(this.casilla) - 1;
        if(posicion > 0){
            this.casilla = historial.getHistorial().get(posicion);
            //this.casilla.setCasilla(historial.getHistorial().get(posicion));
        }
    }
    
    public void avanzar(){
        Integer posicion = historial.posicionEnHistorial(this.casilla) + 1;
        if(posicion <= historial.tamanioHistorial()){
            this.casilla = historial.getHistorial().get(posicion);
        }
    }
    
    public Historial getHistorial (){
        return this.historial;
    }
    
    public Casilla getCasilla(){
        return casilla;
    }
        
    private Boolean movimientoPosible(Casilla casilla){
            if(casilla.getFila() == this.casilla.getFila()+1 && casilla.getColumna() == this.casilla.getColumna()+2){
                    return true;
                }
            if(casilla.getFila() == this.casilla.getFila()-1 && casilla.getColumna() == this.casilla.getColumna()+2){
                    return true;
                }
            if(casilla.getFila() == this.casilla.getFila()-2 && casilla.getColumna() == this.casilla.getColumna()+1){
                    return true;
                }
            if(casilla.getFila() == this.casilla.getFila()-2 && casilla.getColumna() == this.casilla.getColumna()-1){
                   return true;
                }
            if(casilla.getFila() == this.casilla.getFila()-1 && casilla.getColumna() == this.casilla.getColumna()-2){
                    return true;
                }
            if(casilla.getFila() == this.casilla.getFila()+1 && casilla.getColumna() == this.casilla.getColumna()-2){
                   return true;
                }
            if(casilla.getFila() == this.casilla.getFila()+2 && casilla.getColumna() == this.casilla.getColumna()-1){
                   return true;
                }
            if(casilla.getFila() == this.casilla.getFila()+2 && casilla.getColumna() == this.casilla.getColumna()+1){
                   return true;
                }
        return false;
    }
    
    public void actualizarHistorial(Integer cambio){
        historial.actualizarHistorial(cambio);
        //this.casilla = new Casilla(this.casilla.getFila()+cambio, this.casilla.getColumna()+cambio);
    }
    
    public void eliminarEnHistorialNoValidos(){
        historial.eliminarEnHitorialNoValidos(tablero);
    }
    
    public Integer posicionEnHistorial(){
        return historial.posicionEnHistorial(casilla);
    }
    
    public void restaurarValores(){
        if(historial.getHistorial().size()>1){
            historial.borrarEnHistorial(2);
            this.casilla = historial.getHistorial().get(1);
        }
    }
    
}
