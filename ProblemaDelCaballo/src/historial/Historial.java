/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package historial;

import java.util.LinkedHashMap;
import java.util.Map;
import tablero.Tablero;

/**
 *
 * @author luisfer
 */
public class Historial {
    
    private Map<Integer,Casilla> historial;
    
    public Historial (){
        this.historial = new LinkedHashMap();
    }
    
     public void agregarAlHistorial(Casilla casilla){
        historial.put(historial.size()+1, casilla);
    }
    
    public Integer tamanioHistorial(){
        return historial.size();
    }
    
    public void borrarEnHistorial (Integer posicion){
        Integer tope = historial.size();
        for(Integer indice =  posicion ; indice <= tope; indice = indice + 1 ){
            historial.remove(indice);
        }
    }
    
     
    public Boolean buscarEnHistorial(Casilla casilla){
        for(Integer indiceHistorial=1; indiceHistorial <= historial.size(); indiceHistorial ++){
               if(historial.get(indiceHistorial).getFila().equals(casilla.getFila()) && historial.get(indiceHistorial).getColumna().equals(casilla.getColumna()))
                    return true;
        }
        return false;         
    }
    
    public Integer posicionEnHistorial (Casilla casilla){
       for (Integer indice = 1; indice < historial.size(); indice++){
           if(casilla.equals(historial.get(indice)))
               return indice;   
       }
       return historial.size();
    }
    
    public void eliminarEnHitorialNoValidos(Tablero tablero){
       Map<Integer, Casilla> _mapa = new LinkedHashMap(); 
       Integer contador = 1;
       Integer tope = historial.size();
       for (Integer indice = 1; indice <= tope; indice ++){
             if(historial.get(indice).getFila()-1 > tablero.getDimension() || historial.get(indice).getColumna()-1 >tablero.getDimension() || historial.get(indice).getFila()-1 < 1 || historial.get(indice).getColumna()-1 < 1)
                 historial.remove(indice);
             else{
                 _mapa.put(contador, historial.get(indice));
                 contador ++;
             }
       }
       this.historial = _mapa;
    }
    
    public void actualizarHistorial(Integer cambio){
        for(Integer indice = 1; indice <= historial.size(); indice ++){
            historial.get(indice).setCasilla(new Casilla (historial.get(indice).getFila()+cambio, historial.get(indice).getColumna()+cambio));
        }
       
    }
    
    public Map<Integer,Casilla> getHistorial(){
        return historial;
    }
    
    public String toString(){
        String s = "{";
        for(Integer indice = 1; indice < historial.size(); indice ++){
            s = s+historial.toString() +"    ";
        }
        return s += " }";
    }
}
