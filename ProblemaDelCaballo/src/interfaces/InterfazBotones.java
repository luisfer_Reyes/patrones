package interfaces;

import historial.Casilla;
import historial.Historial;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import pieza.Caballo;

public class InterfazBotones extends JPanel {
    private JButton avanzarMovimiento = new JButton();
    private JButton retrocederMovimiento = new JButton();
    private JButton aumentarTamanioTablero = new JButton();
    private JButton disminuirTamanioTablero = new JButton();
    private JButton caballos = new JButton();
    private JButton reiniciarJuego = new JButton();
    private JLabel etiquetaTamanioTablero = new JLabel();
    private InterfazTablero tableroAjedrez;
    
    public InterfazBotones(InterfazTablero tableroAjedrez){
        this.tableroAjedrez = tableroAjedrez;
        
        avanzarMovimiento.setPreferredSize(new Dimension(100, 25));
        retrocederMovimiento.setPreferredSize(new Dimension(100, 25));
        aumentarTamanioTablero.setPreferredSize(new Dimension(45, 25));
        disminuirTamanioTablero.setPreferredSize(new Dimension(40, 25));
                
        avanzarMovimiento.setText("↷");
        retrocederMovimiento.setText("↶");
        aumentarTamanioTablero.setText("+");
        disminuirTamanioTablero.setText("-");
        caballos.setText("Agregar nuevo caballo");
        reiniciarJuego.setText("Reiniciar Juego");
        
        avanzarMovimiento.setEnabled(false);
        retrocederMovimiento.setEnabled(false);
        
        this.setLayout(new GridBagLayout ());
        
        JLabel etiquetaTamanio = new JLabel();
        etiquetaTamanio.setText("Tamaño tablero");

        JLabel etiquetaMovimientos = new JLabel();
        etiquetaMovimientos.setText("Movimientos");
        
        JLabel etiquetaCaballos = new JLabel();
        etiquetaCaballos.setText("Caballos");
        
        
        this.etiquetaTamanioTablero.setText("8x8");
        
        this.add(etiquetaTamanio,agregar(2,0,1,1,true));
        this.add(etiquetaTamanioTablero,agregar(3,0,1,1,false));
        this.add(this.aumentarTamanioTablero,agregar(2,1,1,1,false));
        this.add(this.disminuirTamanioTablero,agregar(3,1,1,1,false));
        this.add(etiquetaMovimientos,agregar(1,3,3,1,true));
        this.add(this.retrocederMovimiento,agregar(2,4,1,1,false));
        this.add(this.avanzarMovimiento,agregar(3,4,1,1,false));
        this.add(etiquetaCaballos,agregar(1,5,3,1,true));
        this.add(this.caballos,agregar(1,6,2,1,false));
        this.add(this.reiniciarJuego,agregar(3,6,2,1,false));
        this.add(new JLabel (),agregar(1,7,1,1,true));
        
        this.retrocederMovimiento.addActionListener(new ActionListener (){
            public void actionPerformed(ActionEvent e){
                regresarEnHistorial();
            }
        });
        
        this.avanzarMovimiento.addActionListener(new ActionListener (){
            public void actionPerformed(ActionEvent e){
                avanzarEnHistorial();
            }
        });
        
        this.aumentarTamanioTablero.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ae) {                      
                            incrementarCasillas();
                    }
        });
        this.disminuirTamanioTablero.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ae) {                      
                            disminuirCasillas();
                    }
        });
        
        this.caballos.addActionListener(new ActionListener (){
            public void actionPerformed(ActionEvent ae){
                agregarCaballo();
            }
        });
        
        this.reiniciarJuego.addActionListener(new ActionListener (){
            public void actionPerformed (ActionEvent  ae){
                reiniciarJuego();
            }
        });
        
    }
    
    private GridBagConstraints  agregar (Integer gridx,Integer gridy, Integer gridWidth, Integer gridheight, Boolean estirar){
        GridBagConstraints contraints = new GridBagConstraints ();
        contraints.gridx = gridx;
        contraints.gridy = gridy;
        contraints.gridwidth = gridWidth;
        contraints.gridheight = gridheight;
        if (estirar)
            contraints.weighty = 1.0;
        return contraints;
    }
    
    private void regresarEnHistorial(){
        if(this.retrocederMovimiento.isEnabled()){
            Caballo caballo = tableroAjedrez.getTablero().getCaballos().get(tableroAjedrez.getJugador());
            Casilla casillaAnterior = caballo.getCasilla();
            if(caballo.posicionEnHistorial()>1){
                caballo.regresar();
                this.tableroAjedrez.despintarCaballo(casillaAnterior);
                this.tableroAjedrez.pintarCaballo(tableroAjedrez.getJugador());
                this.tableroAjedrez.habilitarBoton(casillaAnterior);
            }
        }
    }
    
    private void avanzarEnHistorial(){
        if(this.avanzarMovimiento.isEnabled()){
            Caballo caballo = tableroAjedrez.getTablero().getCaballos().get(tableroAjedrez.getJugador());
            Casilla casillaAnterior = caballo.getCasilla();
            if(caballo.posicionEnHistorial() < caballo.getHistorial().tamanioHistorial()){
                caballo.avanzar();
                this.tableroAjedrez.despintarCaballo(casillaAnterior);
                this.tableroAjedrez.pintarCaballo(tableroAjedrez.getJugador());   
            }
        }
    }
    
    public void estadoBotonAvanzar(Boolean estado){
        this.avanzarMovimiento.setEnabled(estado);
    }
    
    public void estadoBotonRegresar (Boolean estado){
        this.retrocederMovimiento.setEnabled(estado);
    }
    
    private void incrementarCasillas(){
        if(tableroAjedrez.getTablero().getDimension()<26){
            tableroAjedrez.getTablero().aumentarDimension();
        tableroAjedrez.redimencionarBotones(tableroAjedrez.getTablero().getDimension());
        for (Integer indice = 1; indice <= tableroAjedrez.getTablero().getCaballos().size(); indice++ ){
            tableroAjedrez.pintarCaballo(indice);
            tableroAjedrez.pintarHistorial(indice);
        }
        this.etiquetaTamanioTablero.setText(""+this.tableroAjedrez.getTablero().getDimension()+"x"+this.tableroAjedrez.getTablero().getDimension());
        }
    }
    
    private void disminuirCasillas(){
        if(tableroAjedrez.getTablero().getDimension()> 8){
            tableroAjedrez.getTablero().disminuirDimension();
            tableroAjedrez.redimencionarBotones(tableroAjedrez.getTablero().getDimension());
            for (Integer indice = 1; indice <= tableroAjedrez.getTablero().getCaballos().size(); indice++ ){
            tableroAjedrez.pintarCaballo(indice);
            tableroAjedrez.pintarHistorial(indice);}
        }
        this.etiquetaTamanioTablero.setText(""+this.tableroAjedrez.getTablero().getDimension()+"x"+this.tableroAjedrez.getTablero().getDimension());
    }
    
    private Integer indiceFila (char caracter){
        char [] abecedario = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        for (Integer indice = 0; indice < abecedario.length; indice++)
            if((""+abecedario[indice]).equalsIgnoreCase((""+caracter)))
                return indice;
        
        return -1;
    }
    
    private void agregarCaballo (){
        Historial historial = new Historial ();
        String coordenadas = JOptionPane.showInputDialog("Ingrese una Coordenadas\n Ejemplo : A,8");
        if(coordenadas != null){
            String [] arregloCoordenadas = coordenadas.split(",");
            if (arregloCoordenadas.length == 2){
                if(Character.isLetter((arregloCoordenadas[0].charAt(0))) && indiceFila(arregloCoordenadas[0].charAt(0)) <= tableroAjedrez.getTablero().getDimension() &&Integer.parseInt(""+arregloCoordenadas[1]) <= tableroAjedrez.getTablero().getDimension() ){
                Casilla casilla = new Casilla(tableroAjedrez.getTablero().getDimension()-Integer.parseInt(""+arregloCoordenadas[1])+1,(indiceFila(arregloCoordenadas[0].charAt(0)))+1);
                Caballo caballo = new Caballo(casilla,this.tableroAjedrez.getTablero(),historial);
                tableroAjedrez.getTablero().agregarCaballo(caballo);
                tableroAjedrez.pintarCaballo(this.tableroAjedrez.getTablero().getCaballos().size());
                JOptionPane.showMessageDialog(null,"Turno Jugador"+(tableroAjedrez.getJugador()));
                }
            }
        }
    }
    
    private void reiniciarJuego (){
        for(Integer indice = 1; indice <= tableroAjedrez.getTablero().getCaballos().size(); indice ++){
            tableroAjedrez.despintarCaballo(tableroAjedrez.getTablero().getCaballos().get(indice).getCasilla());
            tableroAjedrez.getTablero().getCaballos().get(indice).restaurarValores();
            tableroAjedrez.despintarTablero();
            tableroAjedrez.pintarCaballo(indice);
        }
        tableroAjedrez.setJugador(1);
        JOptionPane.showMessageDialog(null,"Turno del jugador "+tableroAjedrez.getJugador());
    }
}
