package interfaces;

import historial.Casilla;
import historial.Historial;
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import pieza.Caballo;
import tablero.Tablero;

public class InterfazJuego extends JPanel{
    private InterfazBotones interfazBotones;
    private InterfazTablero interfazTablero;
    
    public InterfazJuego (InterfazBotones interfazBotones, InterfazTablero interfazTablero){
        this.interfazBotones = interfazBotones;
        this.interfazTablero = interfazTablero;
        
        this.setLayout(new GridLayout(1,2));
        
        this.add(interfazTablero);
        this.add(interfazBotones);
        
    }
    
    
   /* public static void main (String [] arg ){
        JFrame ventana = new JFrame ("Problema del Caballo");
        Tablero tablero = new Tablero (8);
        InterfazTablero interfazTablero = new InterfazTablero (tablero);
        InterfazBotones interfazBotones = new InterfazBotones (interfazTablero);
        interfazTablero.agregarInterfazBotones(interfazBotones);
        InterfazJuego interfazJuego = new InterfazJuego (interfazBotones,interfazTablero);
        ventana.add(interfazJuego);
        ventana.pack();
        ventana.setExtendedState(JFrame.MAXIMIZED_BOTH);
        ventana.setVisible(true);
        ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }*/
    
}
