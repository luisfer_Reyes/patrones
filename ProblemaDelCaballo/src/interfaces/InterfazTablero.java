package interfaces;

import historial.Casilla;
import historial.Historial;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import pieza.Caballo;
import tablero.Tablero;

public class InterfazTablero extends JPanel{
    private ArrayList<ArrayList<JButton>> botones;
    private Tablero tablero;
    private InterfazBotones interfazBotones;
    private Integer jugador ;
    
    public InterfazTablero(Tablero tablero) {
        this.tablero = tablero;
        this.botones = insertarBotones(this.tablero.getDimension(),this.tablero.getDimension());
        jugador = 1;
       
    }
    
    public ArrayList<ArrayList<JButton>> insertarBotones(Integer f, Integer c){
        char alfabeto[]="ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        ArrayList<ArrayList<JButton>> botonesaux = new ArrayList<>();
        for (Integer indiceFila = 0; indiceFila < f; indiceFila++) {
            ArrayList<JButton> _botones = new ArrayList<>();
            for (Integer indiceColumna = 0; indiceColumna < c; indiceColumna++) {
                _botones.add(new JButton());
                _botones.get(indiceColumna).setPreferredSize(new Dimension(100, 25));
                _botones.get(indiceColumna).setSize(360/f, 360/f);
                _botones.get(indiceColumna).setMargin(new Insets(1, 1, 1, 1));
                 
                if ((indiceFila + indiceColumna + 1) % 2 == 0) 
                    _botones.get(indiceColumna).setBackground(Color.black);
                else
                    _botones.get(indiceColumna).setBackground(Color.white);
                
                JButton botonAuxiliar = _botones.get(indiceColumna);
                botonAuxiliar.setFont(new Font ("TimesRoman", Font.BOLD, 200/f));
                Integer fila = indiceFila, columna = indiceColumna;
                
                
                _botones.get(indiceColumna).addActionListener(new ActionListener (){
                    public void actionPerformed(ActionEvent ae) {
                        if(tablero.getCaballos().size()==1)
                            seleccionCasilla(fila,columna,jugador);
                        else{
                            if(seleccionCasilla(fila,columna,jugador)){
                                jugador ++;
                                if(jugador > tablero.getCaballos().size())
                                    jugador =1;
                                JOptionPane.showMessageDialog(null, "Turno jugador "+ jugador);
                            }
                            
                        }
                        if (!tablero.getCaballos().isEmpty()){
                            despintarTablero();
                            pintarHistorial(jugador);}
                    }
                });
                _botones.get(indiceColumna).setEnabled(true);
                _botones.get(indiceColumna).setText(alfabeto[indiceColumna]+"-"+(c-indiceFila));
                _botones.get(indiceColumna).setForeground(Color.RED);
                this.add(_botones.get(indiceColumna));
            }
           botonesaux.add(_botones);
           
            this.setLayout(new GridLayout(f,c));
        }
        return botonesaux;
    }
      
    private Boolean seleccionCasilla(Integer fila,Integer columna,Integer indiceCaballo){
        if (!tablero.getCaballos().isEmpty()){
            Caballo caballo = tablero.getCaballos().get(indiceCaballo);
            Casilla casillaMovimiento = new Casilla(fila+1,columna+1);
            Boolean movimiento = caballo.mover(casillaMovimiento);

            if(movimiento== true){
                Casilla casillaAnterior = caballo.getHistorial().getHistorial().get(caballo.getHistorial().getHistorial().size()-1);
                despintarCaballo(casillaAnterior);
                pintarCaballo(indiceCaballo);


                if(caballo.getHistorial().tamanioHistorial()== (tablero.getDimension() * tablero.getDimension())){
                    JOptionPane.showMessageDialog(null,"Jugador "+ indiceCaballo+ "ha ganado!");
                    deshabilitarTablero();
                }
                return true;
            }
        }
        return false;
    }
    
    public void despintarCaballo(Casilla casilla){
        botones.get(casilla.getFila()-1).get(casilla.getColumna()-1).setIcon(null);
    }
    
        
    public void habilitarBoton (Casilla casilla){
        JButton boton = botones.get(casilla.getFila()-1).get(casilla.getColumna()-1);
        boton.setEnabled(true);
        if(boton.getBackground().equals(Color.BLUE))
            boton.setBackground(Color.black);
        else
            boton.setBackground(Color.white);
    }
            
    public void pintarCaballo(Integer numeroCaballo){
        Caballo caballo = tablero.getCaballos().get(numeroCaballo);
        JButton boton = botones.get(caballo.getCasilla().getFila()-1).get(caballo.getCasilla().getColumna()-1);
        ImageIcon imagen = new ImageIcon("caballo.png");
        Icon icon = new ImageIcon (imagen.getImage().getScaledInstance(boton.getWidth(), boton.getHeight(), Image.SCALE_DEFAULT));
        boton.setIcon(icon);
        if(boton.getBackground().equals(Color.BLACK)||boton.getBackground().equals(Color.BLUE))
            boton.setBackground(Color.BLUE);
        else
              boton.setBackground(Color.CYAN);
        if(this.interfazBotones != null){
            
            if(caballo.posicionEnHistorial()<caballo.getHistorial().tamanioHistorial())
                this.interfazBotones.estadoBotonAvanzar(true);
            else
                this.interfazBotones.estadoBotonAvanzar(false);
            
            if(caballo.posicionEnHistorial()>1)
                this.interfazBotones.estadoBotonRegresar(true);
            else
                this.interfazBotones.estadoBotonRegresar(false);
        }
    }
    
    public void redimencionarBotones(Integer dimension){
        this.removeAll();
        botones = insertarBotones(dimension, dimension);
    }
    
    public Tablero getTablero(){
        return tablero;
    }
    
    public Integer getJugador(){
        return jugador;
    }
    
    public void setJugador(Integer jugador){
        this.jugador = jugador;
    }
    public void agregarInterfazBotones (InterfazBotones interfazBotones){
        this.interfazBotones = interfazBotones;
    }
 
    public void pintarHistorial (Integer indiceCaballo){
        
        Historial historial = tablero.getCaballos().get(indiceCaballo).getHistorial();
        
        for (Integer indice = 1; indice <= historial.tamanioHistorial(); indice ++){
            Integer fila = historial.getHistorial().get(indice).getFila();
            Integer columna = historial.getHistorial().get(indice).getColumna();
            if(this.botones.get(fila-1).get(columna-1).getBackground().equals(Color.BLACK)||this.botones.get(fila-1).get(columna-1).getBackground().equals(Color.BLUE))
                this.botones.get(fila-1).get(columna-1).setBackground(Color.BLUE);
            else
                this.botones.get(fila-1).get(columna-1).setBackground(Color.CYAN);
        }
    }
    
    public void despintarTablero(){
        for (Integer fila = 0; fila < tablero.getDimension(); fila ++)
            for (Integer columna = 0; columna < tablero.getDimension(); columna ++)
                if(this.botones.get(fila).get(columna).getBackground().equals(Color.BLACK)||this.botones.get(fila).get(columna).getBackground().equals(Color.BLUE))
                    this.botones.get(fila).get(columna).setBackground(Color.BLACK);
                else
                    this.botones.get(fila).get(columna).setBackground(Color.WHITE);
    }
    
    private void deshabilitarTablero(){
        for (Integer fila = 0; fila < tablero.getDimension(); fila ++)
            for (Integer columna = 0; columna < tablero.getDimension(); columna ++)
                    this.botones.get(fila).get(columna).setEnabled(false);
    }
}