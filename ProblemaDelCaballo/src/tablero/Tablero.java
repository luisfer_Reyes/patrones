package tablero;

import pieza.Caballo;
import java.util.LinkedHashMap;
import java.util.Map;


public class Tablero {
    private int dimension;
    private Map<Integer,Caballo> caballos;
    
    public Tablero(int dimension){
        caballos = new LinkedHashMap();
        if(dimension < 8)
            this.dimension=8;
        else
            if(dimension > 26)
                this.dimension = 26;
            else
                this.dimension=dimension;
    }
    
    public void aumentarDimension(){
        if(this.dimension+2  > 26)
            this.dimension=26;
        else{
            this.dimension=this.dimension+2;
            actualizarCaballos(1);
        }
    }
    
    private Boolean buscarPasado(Integer fila, Integer columna, Caballo caballo){
        for(Integer indiceHistorial=1; indiceHistorial <= caballo.posicionEnHistorial(); indiceHistorial ++){
                    if(caballo.getHistorial().getHistorial().get(indiceHistorial).getFila().equals(fila) && caballo.getHistorial().getHistorial().get(indiceHistorial).getColumna().equals(columna))
                        return true;
        }
        return false;         
    }
    
    private boolean afectaCaballosEnFilas(){
        for(Integer indice = 1; indice <= caballos.size(); indice ++){
            if(caballos.get(indice).getCasilla().getFila() == 1 || caballos.get(indice).getCasilla().getColumna() == 1)
                return true;
        }
        return false;
    }
    
    private boolean afectaCaballos(int decremento){
        for(Integer indice = 1; indice <= caballos.size(); indice ++){
            if(caballos.get(indice).getCasilla().getFila()-1 > this.dimension - decremento || caballos.get(indice).getCasilla().getColumna()-1 > this.dimension-decremento)
                return true;
        }
        return false;
    }
    
    private void actualizarCaballos(Integer diferencia){
        for(Integer indice = 1; indice <= caballos.size(); indice ++){
            caballos.get(indice).actualizarHistorial(diferencia);
        }
    }
    
    public void disminuirDimension(){
        if(!afectaCaballos(2) && !afectaCaballosEnFilas()){
            if(this.dimension-2 < 8)
                this.dimension=8;
            else{
                this.dimension=this.dimension-2;
                eliminarCasillasInvalidasEnHistorial();
                actualizarCaballos(-1);
            }
           // eliminarCasillasInvalidasEnHistorial();
        }
        
    }
    
    private void eliminarCasillasInvalidasEnHistorial(){
        for(Integer indice = 1; indice <= caballos.size(); indice++){
            caballos.get(indice).eliminarEnHistorialNoValidos();
        }
    }
     
    public int getDimension(){
        return dimension;
    }
    
    public void agregarCaballo(Caballo caballo){
        this.caballos.put(caballos.size()+1, caballo);
    }
    
    public Map<Integer,Caballo> getCaballos(){
        return caballos;
    }
}
